//
//  HomeTableViewController.swift
//  ChatSnap
//
//  Created by Will Gunby on 19/01/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit
import Parse

class HomeTableViewController: UITableViewController, AsyncInteraction {
    
    @IBOutlet var btnLogout: UIBarButtonItem!
    var interface : ChatSnapSnaps!
    var refreshPull: UIRefreshControl!
    var includeSelf = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        interface = ChatSnapSnaps(asyncHandler: self, validationResultHandler: nil)
        refreshPull = UIRefreshControl()
        refreshPull.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshPull.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        
        self.tableView.addSubview(refreshPull)
        refresh()
    }
    
    var refreshing = false
    override func viewDidAppear(animated: Bool) {
        if refreshing {
            refresh()
        }
    }
    func refresh(){
        refreshing=true
        interface.loadSnaps("viewDidLoad")
        self.refreshPull.endRefreshing()
        refreshing=false
    }
    
    @IBAction func btnLogout_up(sender: AnyObject) {
        PFFacebookUtils.session().close()
        PFUser.logOut()
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interface.snaps.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        let index = indexPath.row
        
        //should be a CodeData model - untyped objects from Parse are not great - could inherit it, but probably just wrap it up to abstract it.
        var snap:PFObject?
        snap = interface.snaps[index]
        println(snap)
        if snap != nil {
            cell.textLabel?.text = snap!["subject"] as? String
            cell.detailTextLabel?.text = (snap!["fromUsername"] as? String)
        }
        
        return cell
    }
    
    
    var selectedIndex = -1
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIndex = indexPath.row
        //go to the next screen
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("ToViewSnap", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToViewSnap" {
            let viewSnap = (segue.destinationViewController as! ViewSnapViewController)
            viewSnap.snap = interface.snaps[selectedIndex]
            selectedIndex = -1
        }
    }
    
    
    
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    
    func beginAsyncWait(callName:String){
        beginAsyncWait()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        if callName == "viewDidLoad.loadSnaps" {
            tableView.reloadData()
        }
        endAsyncWaitWithSuccess()
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        println(error)
        self.endAsyncWaitWithError()
    }
    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 100, 100))
    func beginAsyncWait(){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        self.view.addSubview(spinny)
        spinny.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endAsyncWaitWithSuccess(){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
    }
    func endAsyncWaitWithError(){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
    }

}
