//
//  SnapBoxTableViewController.swift
//  ChatSnap
//
//  Created by Will Gunby on 19/02/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

class ChooseRecipientTableViewController: UITableViewController, AsyncInteraction, UINavigationControllerDelegate {
    
    var interface : ChatSnapUsers!
    var refreshPull: UIRefreshControl!
    var includeSelf = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        interface = ChatSnapUsers(asyncHandler: self, validationResultHandler: nil)
        refreshPull = UIRefreshControl()
        refreshPull.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshPull.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        
        setupBackButton(self)
        self.tableView.addSubview(refreshPull)
        refresh()
    }
    
       
    var refreshing = false
    override func viewDidAppear(animated: Bool) {
        if refreshing {
            refresh()
        }
    }
    
    func refresh(){
        refreshing=true
        interface.loadUsers("viewDidLoad", connectionsOnly: false, includeSelf:includeSelf)
        self.refreshPull.endRefreshing()
        refreshing=false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        println(interface.users.count)
        println(interface.items.count)
        return interface.users.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        let index = indexPath.row
        
        var user:PFUser?
        user = interface.users[index]
        println(user)
        if user != nil {
            cell.textLabel?.text = user!["username"] as? String
        }
        
        return cell
    }
    
    
    var selectedIndex = -1
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //validate user  (could check for ignores/block lists here)
        
        selectedIndex = indexPath.row
        //go to the next screen
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("ToCreateSnap", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToCreateSnap" {
            let createView = (segue.destinationViewController as! CreateViewController)
            createView.recipient = interface.users[selectedIndex]
            selectedIndex = -1
        }
    }
    
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    
    func beginAsyncWait(callName:String){
        beginAsyncWait()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        if callName == "viewDidLoad.loadUsers" {
            tableView.reloadData()
        }
        endAsyncWaitWithSuccess()
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        println(error)
        self.endAsyncWaitWithError()
    }
    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 100, 100))
    func beginAsyncWait(){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        self.view.addSubview(spinny)
        spinny.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endAsyncWaitWithSuccess(){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
    }
    func endAsyncWaitWithError(){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
    }

}
