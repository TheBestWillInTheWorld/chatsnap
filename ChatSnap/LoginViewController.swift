//
//  ViewController.swift
//  Tendir
//
//  Created by Will Gunby on 26/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, AsyncInteraction {

    @IBOutlet var login: UIView!
    @IBOutlet var lblLoginValidation: UILabel!
    
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var newUser: UIView!
    @IBOutlet var txtUsername: UITextField!
    @IBOutlet var lblUsernameValidate: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        self.view.hidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        if PFUser.currentUser() != nil {
            println("logged in already")
            var interface = ChatSnapUsers(asyncHandler: self, validationResultHandler: nil)
            interface.getFacebookProfilePic(PFUser.currentUser(), gotImage: gotImage)
            setupForPush()
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("ToMain", sender: self)
            }
            self.view.hidden = true
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    func gotImage(image:UIImage){
        //image already saved to Parse, but could show it on scheen at this point
    }
    
    
    override func viewDidDisappear(animated: Bool) {
        reset()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //BUG:  iOS FB account seems to prevent login via the Parse method.  With a FB account registered in the OS, it doesn't go to web and returns one of these (at least for "test users" in FB):
    //com.facebook.sdk:ErrorInnerErrorKey=Error Domain=com.apple.accounts Code=7 "The Facebook server could not fulfill this access request: Invalid application 905604816145855" UserInfo=0x7f86450084f0
    @IBAction func btnFacebook_up(sender: AnyObject) {
        var permissions = [] //["public_profile"]
        
        
        UIView.animateWithDuration(0.2,
            animations:{
                self.btnFacebook.alpha = 0
                //self.btnFacebook.hidden = true
            }
        )
        
        
        PFFacebookUtils.logInWithPermissions([])
            { (user:PFUser!, error:NSError!) -> Void in
                if user == nil {
                    println("Uh oh. The user cancelled the Facebook login. \(error)")
                    self.lblLoginValidation.hidden = false
                } else if user.isNew {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.newUser(user)
                    }
                } else {
                    println("User logged in through Facebook!")
                    self.lblLoginValidation.hidden = true
                    self.setupForPush()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("ToMain", sender: self)
                    }
                }
        }
    }
    
    func setupForPush(){
        //do this to aid push notification
        ParseInterface.attachCurrentUserToCurrentInstallation()
    }
    
    @IBAction func btnOk_up(sender: AnyObject) {
        lblUsernameValidate.hidden = true
        //trim username to reduce impersonators!
        txtUsername.text = txtUsername.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        PFUser.currentUser().username = txtUsername.text
        PFUser.currentUser().saveInBackgroundWithBlock { (success:Bool, error:NSError!) -> Void in
            if success {
                //reset in case we come back when the user logs out
                self.setupForPush()
                //move to main screen
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("ToMain", sender: self)
                }
            }
            else {
                //show error
                self.lblUsernameValidate.text = error.userInfo!["error"] as! String!
                self.lblUsernameValidate.hidden = false
            }
        }
    }
    
    func newUser(user:PFUser){
        println("User signed up and logged in through Facebook!")
        self.lblLoginValidation.hidden = true
        
        self.login.hidden = true
        self.newUser.hidden = false
        
        txtUsername.text = ""  //the auto-gen username from Parse is not really usable:   user.username
    }
    
    func reset(){
        self.lblLoginValidation.hidden = true
        
        self.login.hidden = false
        self.newUser.hidden = true
        
        txtUsername.text = ""
        
        self.btnFacebook.alpha = 1
        self.btnFacebook.hidden = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //self.reset()
    }
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 100, 100))
    func beginAsyncWait(callName:String){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        self.view.addSubview(spinny)
        spinny.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
    }
}

