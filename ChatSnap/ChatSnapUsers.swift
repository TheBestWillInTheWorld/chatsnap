//
//  ChatSnapInterface.swift
//  ChatSnap
//
//  Created by Will Gunby on 19/01/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

import Foundation
import Parse


class ChatSnapUsers: ParseInterface {   
    
    var users: [PFUser] {
        get {
            return self.items as! [PFUser]
        }
        set(values) {
            self.items = values
        }
    }
    
    func saveConnection(callName:String, user:PFUser!, remove:Bool, otherUser:PFUser!){
        //http://blog.parse.com/2012/05/17/new-many-to-many/
        var connections = user.relationForKey("connections") as PFRelation
        
        if remove { connections.removeObject  (otherUser)   }
        else      { connections.addObject     (otherUser)   }
        
        let compositeCallname = "\(callName).saveConnection"
        asyncHandler!.beginAsyncWait(compositeCallname)
        user.saveInBackgroundWithBlock { (success:Bool, error:NSError!) -> Void in
            if !success {
                println(error)
                self.asyncHandler!.endAsyncWaitWithError(compositeCallname, error: error)
            }
            else {
                self.asyncHandler!.endAsyncWaitWithSuccess(compositeCallname, data: [user])
            }
        }
    }
    
    //find users using "begins with" (or maybe just allow adding from exact matches...?)
    //list friends?
    
    func loadUsers(callName:String,searchUsersStartingWith:String){
        loadUsers(callName,connectionsOnly:false,searchUsersStartingWith:searchUsersStartingWith, includeSelf:false)
    }
    func loadUsers(callName:String,searchUsersStartingWith:String, includeSelf:Bool){
        loadUsers(callName,connectionsOnly:false, searchUsersStartingWith:searchUsersStartingWith, includeSelf:includeSelf)
    }
    
    func loadUsers(callName:String,connectionsOnly:Bool){
        loadUsers(callName,connectionsOnly:connectionsOnly, includeSelf:false)
    }
    func loadUsers(callName:String,connectionsOnly:Bool, includeSelf:Bool){
        loadUsers(callName,connectionsOnly:connectionsOnly, searchUsersStartingWith:"", includeSelf:includeSelf)
    }
    
    func loadUsers(callName:String,connectionsOnly:Bool,searchUsersStartingWith:String){
        loadUsers(callName,connectionsOnly:connectionsOnly, searchUsersStartingWith:"", includeSelf:false)
    }
    func loadUsers(callName:String,connectionsOnly:Bool,searchUsersStartingWith:String, includeSelf:Bool){
        var query = PFUser.query()
        let currentUser = PFUser.currentUser()
        query.limit = querySize
        query.skip = (currentPage - 1) * querySize
        
        //don't offer self selection - we don't want to pander to narcissists!
        if !includeSelf {  query.whereKey("username", notEqualTo: currentUser.username)    }
        if searchUsersStartingWith as String! != ""  { query.whereKey("username", hasPrefix: searchUsersStartingWith) }
        if connectionsOnly {
            var connections = (currentUser.relationForKey("connections") as PFRelation)
            
            //users in "connections" for other users current user
            query.whereKey("objectId", matchesKey: "objectId", inQuery: connections.query())
            // other people who have us as a conection:     query.whereKey("connections", containedIn: [currentUser])  
        }
        
        asyncHandler!.beginAsyncWait("\(callName).loadUsers")
        query.findObjectsInBackgroundWithBlock { (result:[AnyObject]!, error:NSError!) -> Void in
            if error != nil {
                self.asyncHandler!.endAsyncWaitWithError("\(callName).loadUsers", error:error)
            }
            else {
                if result.count > 0 {
                    //keep only one page of users in memory
                    self.users = result as! [PFUser]!
                    self.asyncHandler!.endAsyncWaitWithSuccess("\(callName).loadUsers", data:result)
                }
                else {
                    let pageNumberWas = self.currentPage
                    //no users found :( - reset
                    self.users = [PFUser]()
                    self.currentPage = 1
                    self.index = 0
                    
                    //if we got no results and the current page >1, we're off the end of the result set to cycle back to Page 1 and try again
                    if pageNumberWas>1  {
                        self.loadUsers(callName, connectionsOnly:connectionsOnly, searchUsersStartingWith:searchUsersStartingWith, includeSelf:includeSelf)
                    }
                    else {
                        self.asyncHandler!.endAsyncWaitWithSuccess("\(callName).loadUsers", data:result)
                    }
                }
            }
        }
    }
        
    
    
}
