//
//  CreateViewController.swift
//  ChatSnap
//
//  Created by Will Gunby on 30/01/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

class CreateViewController: UIViewController, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, AsyncInteraction, ValidationResult {

    @IBOutlet var imgSnap: UIImageView!
    @IBOutlet var lblRecipient: UILabel!
    @IBOutlet var txtSubject: UITextView!
    @IBOutlet var txtMessage: UITextView!
    
    var recipient:PFUser!
    var notiMan = NotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        lblRecipient.text = recipient.username
        
        var imgTap = UITapGestureRecognizer(target: self, action: "imgSnap_tapped:")
        
        imgSnap.addGestureRecognizer(imgTap)
        txtSubject.delegate = self
        txtMessage.delegate = self
        txtSubject.text = initialSubjectText
        txtMessage.text = initialMessageText
        kbdHeight = 0
        
        setupBackButton(self)
    }
    @IBOutlet var scrollView: UIScrollView!
        
    func validationFailed(callName:String, validationFailures:[validationFailure]){
        var userMessage = ""
        for fail in validationFailures {
            println(fail)
            userMessage += fail.failureUserMessage
        }
        alert("You missed a bit!", message: userMessage)
    }
    
    func userSubjectText() -> String{
        if txtSubject.text == initialSubjectText { return ""}
        return txtSubject.text
    }
    
    func userMessageText() -> String{
        if txtMessage.text == initialMessageText { return ""}
        return txtMessage.text
    }
    
    
    func alert(title:String, message:String){
        alert(self, title: title, message: message) { (action:UIAlertAction!) -> Void in
        }
    }
    func alert(title:String, message:String, closure:(UIAlertAction!) -> Void ){
        alert(self, title: title, message: message, closure: closure)
    }
    func alert(containerViewController:UIViewController, title:String, message:String, closure:(UIAlertAction!) -> Void ){
        
        var alert = UIAlertController(title:title, message: message, preferredStyle: UIAlertControllerStyle.Alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: closure))
        
        containerViewController.presentViewController(alert, animated: true) { () -> Void in }
    }
    
    @IBAction func btnSend_up(sender: AnyObject) {
        self.view.endEditing(true)
        let interface = ChatSnapSnaps(asyncHandler: self, validationResultHandler: self)
        let imgData = UIImagePNGRepresentation(self.imgSnap.image)
        interface.sendSnap("btnSend_up", sender:PFUser.currentUser(), recipient: recipient, subject: userSubjectText(), message: userMessageText(), imageData: imgData)
    }
    
    //<text entry>
    //Keyboard delegates
    override func viewWillAppear(animated: Bool) {
        notiMan.registerObserver(UIKeyboardWillShowNotification, block: keyboarWillAppear)
        notiMan.registerObserver(UIKeyboardWillHideNotification, block: keyboarWillDisappear)
    }
    override func viewWillDisappear(animated: Bool) {
        notiMan.deregisterAll()
    }
    var keyboardShowing = false
    var kbdHeight:CGFloat!
    func keyboarWillAppear(notification:NSNotification!){
        keyboardShowing = true
        
        let info = notification.userInfo as NSDictionary?
        let rectValue = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let kbSize = rectValue.CGRectValue().size
        
        kbdHeight = kbSize.height
    }
    func keyboarWillDisappear(notification:NSNotification!){
        keyboardShowing = false
        scrollView.setContentOffset(CGPointMake(0, 0), animated: true )
    }
    func textViewDidBeginEditing(textView: UITextView) {
        //reset to ensure no double scrollups
        scrollView.setContentOffset(CGPointMake(0, 0), animated: true )
        
        let viewport = CGRectMake(0, 0, scrollView.frame.width, scrollView.frame.height - kbdHeight)
        println("\(viewport) - \(textView.frame)")
        if !viewport.contains(textView.frame) {
            //as this is a static set of content that is smaller than the screen height, we just shift by the KBS height
            scrollView.setContentOffset(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y + kbdHeight!), animated: true )
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
    }
    let initialSubjectText = "Subject"
    let initialMessageText = "Add a message here!"
    func textViewShouldBeginEditing(textView: UITextView) -> Bool{
        var initialText = ""
        switch textView {
        case txtMessage:
            initialText = initialMessageText
        case txtSubject:
            initialText = initialSubjectText
        default :
            initialText = ""
        }
        
        if textView.text == initialText {
            textView.text = ""
            textView.textColor = UIColor.darkGrayColor()
        }
        return true
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool{
        var initialText = ""
        switch textView {
        case txtMessage:
            initialText = initialMessageText
        case txtSubject:
            initialText = initialSubjectText
        default :
            initialText = ""
        }
        
        if textView.text == "" {
            textView.text = initialText
            textView.textColor = UIColor.lightGrayColor()
        }
        return true
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    //</text entry>
    
    
    //<image selection>
    var imageSource : UIAlertController?
    var imgSize:CGSize?
    func imgSnap_tapped(recognizer:UITapGestureRecognizer){
        if keyboardShowing {
            self.view.endEditing(true)
        }
        else{
            btnPickImage_up(imgSnap)
        }
    }
    @IBAction func btnPickImage_up(sender: AnyObject) {
        
        imageSource = UIAlertController(title: "Hey!", message: "Where do you want to source the image?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        //only offer camera if it's available on the device
        if  UIImagePickerController.isSourceTypeAvailable(.Camera) {
            imageSource!.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
                self.beginWait()
                self.pickImage(UIImagePickerControllerSourceType.Camera)
                self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
            }))
        }
        imageSource!.addAction(UIAlertAction(title: "Photos", style: .Default, handler: { action in
            self.beginWait()
            self.pickImage(UIImagePickerControllerSourceType.PhotoLibrary)
            self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        if imageSource!.actions.count>1{
            imageSource!.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
                self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(imageSource!, animated: true, completion: nil)
        }
        else {
            imageSource = nil
            //we always offer photo's, so if there's only one action, it is photos - got straight there
            self.beginWait()
            self.pickImage(UIImagePickerControllerSourceType.PhotoLibrary)
        }
    }
    func pickImage(source:UIImagePickerControllerSourceType){
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        picker.allowsEditing = true
        //imgSize = CGSize(width: 10,height: 10) //
        imgSize = imgSnap.frame.size
        println(imgSize)
        self.presentViewController(picker, animated: true, completion: {self.endWait()})
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        imgSnap.image = image
        //remove the placeholder background once we've selected an image
        imgSnap.backgroundColor = UIColor.clearColor()
        println(imgSize)
        imgSnap.frame.size = imgSize!
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //</image selection
    
    func confirmSent(){
        alert("Snap sent to \(recipient.username)!", message: "They'll get your Snap next time they check!") { (action:UIAlertAction!) -> Void in
            self.clearForm()
            self.returnToList()
        }
    }
    
    
    func clearForm(){
        txtMessage.text = initialMessageText
        txtSubject.text = initialSubjectText
        txtMessage.textColor = UIColor.darkGrayColor()
        txtSubject.textColor = UIColor.darkGrayColor()
    }
    
    func returnToList(){
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
    
    func beginAsyncWait(callName:String){
        beginWait()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        if callName == "btnSend_up.sendSnap" {
            endWait()
            confirmSent()
        }
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        endWait()
    }
    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 100, 100))
    func beginWait(){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        self.view.addSubview(spinny)
        spinny.startAnimating()
        //this would be a bit crappy if there's no connection!
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endWait(){
        spinny.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
}
