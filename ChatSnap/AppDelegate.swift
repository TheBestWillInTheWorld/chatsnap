//
//  AppDelegate.swift
//  ChatSnap
//
//  Created by Will Gunby on 19/01/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        
        Parse.setApplicationId("3LuOSgp3HBOJksgO3YRxa9WtxG7epytVYCJPdRi0", clientKey:"DXkU8JXaR9cmhTTQenTj0ATe2tzxf2q3G5EC3Uo8")
        
        PFFacebookUtils.initializeFacebook()
        PFInstallation.currentInstallation().save()
        
        //extract push notification
        extractPush(launchOptions)
        
        var pushSettings = UIUserNotificationSettings(forTypes: .Alert | .Sound | .Badge, categories: nil)
        application.registerUserNotificationSettings(pushSettings)
        application.registerForRemoteNotifications()
        
        return true
    }
    
    //https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Introduction.html#//apple_ref/doc/uid/TP40008194-CH103-SW1
    func extractPush(userInfo: [NSObject: AnyObject]?){
        if let info = userInfo {
            var snapObjectId = info["snapObjectId"] as! String
            var snap = PFObject(withoutDataWithClassName: "Snap", objectId: snapObjectId)
            
            // Fetch photo object
            snap.fetchIfNeededInBackgroundWithBlock({ (object:PFObject!, error:NSError!) -> Void in
                // Show photo view controller
                if !(error == nil) {
                    println(error)
                    return
                }
                println(snap)
                if let user = PFUser.currentUser() {
                    if let imgFile = object.valueForKey("imageFile") as! PFFile?{
                        
                        imgFile.getDataInBackgroundWithBlock({ (data:NSData!, error:NSError!) -> Void in
                            if !(error == nil) {
                                println(error)
                                return
                            }
                            println(snap)
                            //show a view based on the content
                            var newVC:ViewSnapViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ViewSnap") as! ViewSnapViewController
                            newVC.snap = snap
                            newVC.image = UIImage(data: data)
                            
                            var vc = self.window?.rootViewController as! UINavigationController
                            //ensure we're at the existing table view in the stack (known to be the 2nd view controller) before continuing
                            //rude, but just experimenting with waht we can acheive from a push message.
                            var list = vc.viewControllers[1] as! HomeTableViewController
                            list.refresh()
                            vc.popToViewController(list, animated: true)
                            vc.pushViewController(newVC, animated: true)
                            
                        })
                    }
                }
            })
            //}
        }
        println(userInfo)
        //PFPush.handlePush(userInfo)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        extractPush(userInfo)
        //may need to push the completionHandler into the extraction routine to handle it async.
        completionHandler(UIBackgroundFetchResult.NoData)
    }
    
    //This is needed to ensure the callback comes through when using PFFacebookUtils (+ minimal framework reverences via CocoaPods) to log in to FB - I suspect that without this, or some of the additional frameworks usually recommended when using PFFacebookUtils, that the FB integration in the OS (or just the FB SDK that's also referenced) takes over, so its session is what gets authorisation, rather than the PFFacebookUtils version....   ....plumbing it in here means the events come back to the PFFacebookUtils object.
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        return FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication, withSession: PFFacebookUtils.session())
    }
    
    func warnBlockingOperationOnMainThread(){
        
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        println("Apple push registration success")
        var currentInstallation = PFInstallation.currentInstallation()
        currentInstallation.setDeviceTokenFromData(deviceToken)
        
        currentInstallation.saveInBackgroundWithBlock { (success:Bool, error:NSError!) -> Void in
            if !success {println(error)}
            else {println("Parse push registration success")}
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        println("Push registration FAIL \(error) ")
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

