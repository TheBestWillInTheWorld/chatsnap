
## Alt interfaces

A combined friend & inbox view might be good.

Group the users that have inboind messages and have an expandable list it there are multiple snaps; each with a link to view the snap

._______________________________________.
|                                       |
|				----------				|
|										|
|   ---------------------------------   |
|  |    Inbox                        |  |
|  |  --------------------------     |  |
|  | |  O O   | @MissyMiss      |  ^ |  |
|  | |   |    |                 |    |  |
|  | |  ___   |    "Hey there"  |    |  |
|  | |                          |    |  |
|  | | [Send Snap] [(5) Snaps]  |    |  |
|  |  --------------------------     |  |
|  |                                 |  |
|  |  --------------------------     |  |
|  | |  ()()  | @MrMan          |    |  |
|  | |    \   |                 |    |  |
|  | |  ___   |    "Check this" |    |  |
|  | |                          |    |  |
|  | | [Send Snap] [(5) Snaps]  |  v |  |
|  |===============================  |  |
|  |    Contacts                     |  |
|  |                                 |  |
|  |  --------------------------     |  |
|  | |  @Bob                    |  ^ |  |
|  | |                          |    |  |
|  |  --------------------------     |  |
|  | |  @Alice                  |    |  |
|  | |                          |    |  |
|  |  --------------------------     |  |
|  | |  @Charlie                |    |  |
|  | |                          |    |  |
|  |  --------------------------     |  |
|  | |  @Deb                    |  v |  |
|   ---------------------------------   |
|										|
|										|
|				(   []   )              |
|										|
l_______________________________________j


### Further UX/UI tweaks
Contacts could be a slide-out with a search box, but the original seperation of inbox/contacts as navigation items seems quite wasteful/long-winded.

That said, an empty inbox is a bit bleak as an entry point, so we need to show the user something to get them started. That could be:
* logo/branding (a panel above the table view with instructions etc)
* Google-esque first-use hints, highlighting the key controls
* additional calls to action in the main content window:    find friends, import contacts, invite, compose

The composition view (CreateSnap) might be better via a different segue than "Show".  Show gives us the nav bar, but constrains the viewport and doesn't present effective branding, leaving the app looking a but anonymous and potentially leaving the user feeling a bit lost/unable to see what app they are in if they've left it open on certain screens.



## Limitations

* single sender-focussed:  select user before composing
** consider group definitions, multi-selct and [+] recipient in the composition screen

* If we recieve a picture as a share from another app (via app Extensions), the compose screen comes before recipient selection

## Data sync & offline modes
* needs to gracefully handle offline modes
* needs to have some degree of local caching to make it vaguely usable/at least approachable while offline
* needs some kind of background sync process & outbound queueing

##TODO's

* TODO: still need to do something better with the push messages - need to play around with the background & foreground behaviour, as well as an inbox screen.  current behaviour of showing the snap without asking the user is crappy, but I just wanted to see what I could do!
* TODO: - this should all be driven by an inbox that is checked when the user activates the app to handle non-active & non running states.  If the user is in the app, a self-dismissing alert/flash to show there's a new item should be the queue for the user to visit the inbox.  the alert could contain a button to jump straight to the smap too, to give the user the option of a shortcut.
* TODO: the app state needs to know whether push registration (iOS and Parse) has suceeded (parse state can be stored perminently, iOS state needs to be re-checked each time as the user can switch it off - if it's switched off in iOS, we should update Parse installation to match).  If it's not saved, then push messages can't be recieved via Parse!
* TODO: groupmessage
* TODO: scrollpaging
* TODO: disable/hide send button intile input complete & indicate required fields
* TODO: add bottom bar to select beween all, freinds and search
* TODO: data retention
* TODO: clearing data of viewed images


* CONSIDER: push from server is safer as enabling client push means the API is open to abuse...
* CONSIDER: allowing the sender to provide an expiry date so the receiver has a countdown!
* CONSIDER: psuh message could contain encryption key for client-side decryption.  not ideal as the server can see that conversation, but an added layer to prevent the image being stored in the clear...

