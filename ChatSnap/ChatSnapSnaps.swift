//
//  ChatSnapSnaps.swift
//  ChatSnap
//
//  Created by Will Gunby on 21/02/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation


class ChatSnapSnaps: ParseInterface {
    
    var snaps : [PFObject] {
        get {
            return items as! [PFObject]
        }
        set(values) {
            self.items = values
        }
    }
    
    func validateSnap(callName:String, user:PFUser!, otherUser:PFUser!, subject:String, message:String, imageData:NSData!) -> [validationFailure]! {
        
        var validation = [validationFailure]()
        if subject == "" { validation.append(validationFailure(failedObject: subject, failedField: "self", failedValue: "", failureReason: "Expected parameter 'subject' was empty", failureUserMessage:"Please enter a subject.")) }
        if imageData == nil || imageData.length == 0 { validation.append(validationFailure(failedObject: imageData, failedField: "self", failedValue: nil, failureReason: "Expected parameter 'imageData' was nil", failureUserMessage:"Please select an image.")) }
        
        return validation
    }
    
    //send snap (save pic for recipient on server & send a push message)
    func sendSnap(callName:String, sender:PFUser!, recipient:PFUser!, subject:String, message:String, imageData:NSData!) -> Bool{
        var validation = validateSnap(callName, user: sender, otherUser: recipient, subject: subject, message: message, imageData: imageData  )
        if validation.count>0{
            if validationResultHandler != nil{
                self.validationResultHandler!.validationFailed(callName, validationFailures: validation)
            }
            return false
        }
        
        let compositeCallname = "\(callName).sendSnap"
        //save users' connection
        let users = ChatSnapUsers(asyncHandler: asyncHandler, validationResultHandler:validationResultHandler)
        users.saveConnection(compositeCallname, user:sender, remove:false, otherUser:recipient)
        //[snap] is not dependent on [connection] (might be dangerous/allow slips), so can be saved async to the snap
        
        var snap = PFObject(className: "Snap")
        snap["subject"] = subject
        snap["imageFile"] = PFFile(data: imageData)
        snap["message"] = message
        snap["fromUsername"] = sender.username    //save username as seperate column to prevent need for a double call to parse & to fetch entire user record to get a username
        snap["from"] = sender
        snap["to"] = recipient
        
        //restrict permissions.
        //ideally, permissions would not allow access to the image or message parts until the timer starts!
        //as it stands, one could write something to constantly poll and retrieve any Snaps for one's user and download them to keep...
        
        snap.ACL = PFACL(user: sender)
        snap.ACL.setReadAccess(true, forUser: recipient)
        snap.saveInBackgroundWithBlock { (success:Bool, error:NSError!) -> Void in
            if !success {
                println(error)
                self.asyncHandler!.endAsyncWaitWithError(compositeCallname, error: error)
            }
            else {
                //send poosh
                self.sendPush(sender, recipient:recipient, message: "'\(subject)'\n\(sender.username) has a Snap for you!", snapObjId: snap.objectId)
                self.asyncHandler!.endAsyncWaitWithSuccess(compositeCallname, data: [snap])
            }
        }
        return true
    }
    
    func sendPush(sender:PFUser!, recipient:PFUser!, message:String, snapObjId:String) {
        let data = [
            "message"         : message,
            "badge"         : "Increment",
            "sound"         : "cheering.caf",           //sound doesn't seem to activate.
            "actionList"    : ["toInbox","toSnap"],
            "snapObjectId"  : snapObjId,
            "from"          : sender.username
        ]
        sendPush(sender, recipient: recipient, message: message, data: data as [NSObject : AnyObject])
    }

    
    
    func loadSnaps(callName:String){
        loadSnaps(callName, includeHistory:false)
    }
    func loadSnaps(callName:String, includeHistory:Bool){
        loadSnaps(callName, fromUser:nil, includeHistory:includeHistory)
    }
    
    func loadSnaps(callName:String, fromUser:PFUser?){
        loadSnaps(callName, fromUser:nil, includeHistory:false)
    }
    func loadSnaps(callName:String,fromUser:PFUser?, includeHistory:Bool){

        var query = PFQuery(className: "Snap")
        let currentUser = PFUser.currentUser()
        query.limit = querySize
        query.skip = (currentPage - 1) * querySize
        
        //by default exclude previous messages, but (maybe) allow subjects to be kept as memories?
        if !includeHistory {  query.whereKey("spent", notEqualTo: true)    }
        //optionally filter to snaps from just one user
        if fromUser != nil {  query.whereKey("from", equalTo: fromUser)    }
        
        //Permissions mean the Snaps are auto-filtered to just mine.
        
        asyncHandler!.beginAsyncWait("\(callName).loadSnaps")
        query.findObjectsInBackgroundWithBlock { (result:[AnyObject]!, error:NSError!) -> Void in
            if error != nil {
                self.asyncHandler!.endAsyncWaitWithError("\(callName).loadSnaps", error:error)
            }
            else {
                if result.count > 0 {
                    //keep only one page of users in memory
                    self.snaps = result as! [PFObject]!
                    self.asyncHandler!.endAsyncWaitWithSuccess("\(callName).loadSnaps", data:result)
                }
                else {
                    let pageNumberWas = self.currentPage
                    //no users found :( - reset
                    self.items = [PFUser]()
                    self.currentPage = 1
                    self.index = 0
                    
                    //if we got no results and the current page >1, we're off the end of the result set to cycle back to Page 1 and try again
                    if pageNumberWas>1  {
                        self.loadSnaps(callName, fromUser:fromUser, includeHistory:includeHistory)
                    }
                    else {
                        self.asyncHandler!.endAsyncWaitWithSuccess("\(callName).loadSnaps", data:result)
                    }
                }
            }
        }
    }
    
    
    //probably needs an inbox...
    //retrieve pic on time limit
    ////on retreive, after viewing, offer to link with them (initially, the link is only from sender to recipient) + offer reply (reply will add a link anyway, but they may just want to save the user for later)
}