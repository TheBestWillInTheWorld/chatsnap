//
//  ViewSnapViewController.swift
//  ChatSnap
//
//  Created by Will Gunby on 14/02/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

class ViewSnapViewController: UIViewController , TimerTick {
    
    @IBOutlet var imgSnap: UIImageView!
    @IBOutlet var lblSender: UILabel!
    @IBOutlet var txtSubject: UITextView!
    @IBOutlet var txtMessage: UITextView!
    @IBOutlet var lblCountdown: UILabel!
    
    var snap : PFObject!
    var image: UIImage!
    var timer: Stopwatch!
    var displayTimeSecs = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println(snap["subject"]!)
        txtSubject.text = snap["subject"]! as! String
        txtMessage.text = snap["message"]! as! String
        lblSender.text = (snap!["fromUsername"] as? String)
        imgSnap.image = image

        setupBackButton(self)
        fetchImgIfAbsent()
        timer = Stopwatch(tickHandler: self)
        lblCountdown.text = "\(displayTimeSecs)"
        timer.start()
    }
    
    func fetchImgIfAbsent(){
        if image == nil {
            if let imgFile = snap.valueForKey("imageFile") as! PFFile?{
                
                imgFile.getDataInBackgroundWithBlock({ (data:NSData!, error:NSError!) -> Void in
                    if !(error == nil) {
                        println(error)
                        return
                    }
                    self.image = UIImage(data: data)
                    self.imgSnap.image = self.image
                })
            }
        }
    }
    
    func tick(milliseconds: Int, seconds: Int, minutes: Int) {
        lblCountdown.text = "\(displayTimeSecs - seconds)"
        if seconds>=displayTimeSecs {
            timer.pause()
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
