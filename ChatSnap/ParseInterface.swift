//
//  ParseInterface.swift
//  ChatSnap
//
//  Created by Will Gunby on 21/02/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//


import Foundation
import Parse


//using @objc to allow an optional func
@objc protocol AsyncInteraction{
    func beginAsyncWait(callName:String)
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?)
    func endAsyncWaitWithError(callName:String, error: NSError)
    optional func gotNewUser(userImage:UIImage,name:String)
    //TODO: find a way to get current method name in order to genericise the callName parameter passed back up the chain
}

protocol ValidationResult{
    //obj, field, value, reason
    func validationFailed(callName:String, validationFailures:[validationFailure])
}

class validationFailure{
    var failedObject:AnyObject?
    var failedField:String
    var failedValue:AnyObject?
    var failureReason:String
    var failureUserMessage:String
    
    init( failedObject:AnyObject?
        ,failedField:String
        ,failedValue:AnyObject?
        ,failureReason:String
        ,failureUserMessage:String
        )
    {
        self.failedObject = failedObject
        self.failedField = failedField
        self.failedValue = failedValue
        self.failureReason = failureReason
        self.failureUserMessage = failureUserMessage
    }
}

class ParseInterface: NSObject {
    
    var itemsHaveLoadedOnce = false
    var items = [AnyObject]()
    let querySize = 10
    var currentPage = 1
    var index = 0
    
    var asyncHandler: AsyncInteraction!
    var validationResultHandler: ValidationResult? //note: I'm not looking at seperating the validation logic from the bae class just yet - this is just ensuring the validation can occur in the base class rather than being repeated in every UI/consumer class
    init(asyncHandler: AsyncInteraction!,validationResultHandler: ValidationResult?){
        self.asyncHandler = asyncHandler
        self.validationResultHandler = validationResultHandler
    }
    
    class func attachCurrentUserToCurrentInstallation(){
        //this is used to do facilitate Push messages to individual users based on targetting installations tied to their users.
        PFInstallation.currentInstallation().setObject(PFUser.currentUser(), forKey: "user")
        PFInstallation.currentInstallation().save()
    }
    
    func getFacebookProfilePic(user:PFUser!, gotImage:(image:UIImage)->()) {
        var imageFile = user["imageFile"] as! PFFile?
        if (imageFile == nil) {
            var FBSession = PFFacebookUtils.session()
            var token = FBSession.accessTokenData.accessToken
            let url = NSURL(string: "https://graph.facebook.com/v2.2/me/picture?type=large&return_ssl_resources=1&access_token=\(token)")
            
            var request = NSURLRequest(URL: url!)
            NSURLConnection.sendAsynchronousRequest(request, queue: .mainQueue(), completionHandler: { (response:NSURLResponse!, data:NSData!, error:NSError!) -> Void in
                gotImage(image: UIImage(data: data)!)
                user["imageFile"] = PFFile(data: UIImageJPEGRepresentation(UIImage(data: data), 0.05))
                user.save()
            })
        }
        else {
            imageFile!.getDataInBackgroundWithBlock({ (data:NSData!, error:NSError!) -> Void in
                gotImage(image: UIImage(data: data)!)
            })
        }
    }
    
    //find users using "begins with" (or maybe just allow adding from exact matches...?)
    
    //list friends
    
    func loadItems(callName:String,searchUsersStartingWith:String){
        loadItems(callName,connectionsOnly:false, includeSelf:false)
    }
    func loadItems(callName:String,searchUsersStartingWith:String, includeSelf:Bool){
        loadItems(callName,connectionsOnly:false, searchUsersStartingWith:searchUsersStartingWith, includeSelf:includeSelf)
    }
    
    func loadItems(callName:String,connectionsOnly:Bool){
        loadItems(callName,connectionsOnly:connectionsOnly, includeSelf:false)
    }
    func loadItems(callName:String,connectionsOnly:Bool, includeSelf:Bool){
        loadItems(callName,connectionsOnly:connectionsOnly, searchUsersStartingWith:"", includeSelf:includeSelf)
    }
    
    func loadItems(callName:String,connectionsOnly:Bool,searchUsersStartingWith:String){
        loadItems(callName,connectionsOnly:connectionsOnly, searchUsersStartingWith:"", includeSelf:false)
    }
    func loadItems(callName:String,connectionsOnly:Bool,searchUsersStartingWith:String, includeSelf:Bool){
        var query = PFUser.query()
        let currentUser = PFUser.currentUser()
        query.limit = querySize
        query.skip = (currentPage - 1) * querySize
        
        //don't offer self selection - we don't want to pander to narcissists!
        if !includeSelf {  query.whereKey("username", notEqualTo: currentUser.username)    }
        if searchUsersStartingWith as String! != ""  { query.whereKey("username", hasPrefix: searchUsersStartingWith) }
        if connectionsOnly {
            var connections = (currentUser.relationForKey("connections") as PFRelation)
            
            //users in "connections" for other users current user
            query.whereKey("objectId", matchesKey: "objectId", inQuery: connections.query())
            // other people who have us as a conection:     query.whereKey("connections", containedIn: [currentUser])
        }
        
        asyncHandler!.beginAsyncWait("\(callName).loadUsers")
        query.findObjectsInBackgroundWithBlock { (result:[AnyObject]!, error:NSError!) -> Void in
            if error != nil {
                self.asyncHandler!.endAsyncWaitWithError("\(callName).loadUsers", error:error)
            }
            else {
                if result.count > 0 {
                    let resultUsers = result as! [PFUser]!
                    //keep only one page of users in memory
                    self.items = resultUsers
                    self.asyncHandler!.endAsyncWaitWithSuccess("\(callName).loadUsers", data:result)
                }
                else {
                    let pageNumberWas = self.currentPage
                    //no users found :( - reset
                    self.items = [PFUser]()
                    self.currentPage = 1
                    self.index = 0
                    
                    //if we got no results and the current page >1, we're off the end of the result set to cycle back to Page 1 and try again
                    if pageNumberWas>1  {
                        self.loadItems(callName, connectionsOnly:connectionsOnly, searchUsersStartingWith:searchUsersStartingWith)
                    }
                    else {
                        self.asyncHandler!.endAsyncWaitWithSuccess("\(callName).loadUsers", data:result)
                    }
                }
            }
        }
    }
    
    //probably needs an inbox...
    //retrieve pic on time limit
    ////on retreive, after viewing, offer to link with them (initially, the link is only from sender to recipient) + offer reply (reply will add a link anyway, but they may just want to save the user for later)
    
    
    
    func sendPush(sender:PFUser!, recipient:PFUser!, message:String, data: [NSObject : AnyObject]) {        
        // Create our installation query
        var pushQuery = PFInstallation.query()
        pushQuery.whereKey("user", equalTo: recipient)
        //restrict to single devicetype:    pushQuery.whereKey("deviceType", equalTo: "android")
        
        // Send push notification to query
        var push = PFPush()
        push.setQuery(pushQuery) // Set our installation query
        push.setData(data)
        push.sendPushInBackgroundWithBlock { (success:Bool, error:NSError!) -> Void in
            println(error)
        }
        //NOTE: push setup involves a cer, a dev cert (installed in xCode and uploaded to Parse) + a provisioning profile.  This helps a lot for Apple setup!  https://parse.com/tutorials/ios-push-notifications + this for coding for Parse https://parse.com/docs/push_guide#top/iOS
        //NOTE: a push to an individual only seems to get received on the provisioned device, the simulator's not getting it....
        //pARSE: need to manage a local store due - Parse is a bit half arsed in some respects - there's a "saveEventually" call on most things, but not on push messages, so if push is part of your workflow, tyou have to store things locally for sending later = coreData needed with status maintenance and async sending.   Also needs a queue of incoming messages :(
    }
    
}
    


