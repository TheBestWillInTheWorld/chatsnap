//
//  MiscGUI.swift
//  ChatSnap
//
//  Created by Will Gunby on 21/02/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation



func setupBackButton(view:UIViewController!){
    if let nav = view.navigationController{
        if var item = nav.navigationBar.backItem{
            var attributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
            //var button = UIBarButtonItem(title: "YourTitle", style: UIBarButtonItemStyle.Bordered, target: view, action: "goBack")
            //button.setTitleTextAttributes(attributes, forState: .Normal)
            //button.tintColor = UIColor.whiteColor()
            //can't seem to set colour or 'out here
            
            view.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
            
        }
        
    }
}